import { Component, OnInit } from '@angular/core';
import * as utils from "tns-core-modules/utils/utils"
import { AppComponent } from "../app.component";

@Component({
  selector: 'ns-my-home',
  templateUrl: './my-home.component.html',
  styleUrls: ['./my-home.component.scss']
})
export class MyHomeComponent implements OnInit {
  constructor() { }

  ngOnInit() {
  }

  onIdk() {
      AppComponent.onFirebaseInfo();
  }



}
