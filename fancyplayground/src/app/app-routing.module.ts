import { NgModule } from "@angular/core";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { Routes } from "@angular/router";

import { MyButtonComponent } from "./my-button/my-button.component"
import { MyHomeComponent } from "./my-home/my-home.component"
import { MyQuizComponent } from "./my-quiz/my-quiz.component";
import { MyCalenderComponent } from "./my-calender/my-calender.component";
import { MyWorkComponent } from "./my-work/my-work.component";
import { MyUniComponent } from "./my-uni/my-uni.component";

const routes: Routes = [
    { path: "", component: MyHomeComponent},
    { path: "home", component: MyWorkComponent },
    { path: "uni", component: MyUniComponent },
    { path: "quiz", component: MyQuizComponent},
    { path: "calender", component: MyCalenderComponent},
    { path: "work", component: MyWorkComponent}
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }
