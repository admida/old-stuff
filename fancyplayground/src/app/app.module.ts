import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptUICalendarModule } from "nativescript-ui-calendar/angular";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";

// --- Firebase shit ---
// import { AngularFireModule } from '@angular/fire';
// import { AngularFirestoreModule } from '@angular/fire/firestore'


// Uncomment and add to NgModule imports if you need to use two-way binding
// import { NativeScriptFormsModule } from "nativescript-angular/forms";


// Uncomment and add to NgModule imports if you need to use the HttpClient wrapper
// import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { MyButtonComponent } from './my-button/my-button.component';
import { MyHomeComponent } from './my-home/my-home.component';
import { MyQuizComponent } from './my-quiz/my-quiz.component';
import { MyCalenderComponent } from './my-calender/my-calender.component';
import { MyWorkComponent } from './my-work/my-work.component';
import { MyUniComponent } from './my-uni/my-uni.component';

@NgModule({
    bootstrap: [
        AppComponent,
    ],
    imports: [
        NativeScriptModule,
        AppRoutingModule,
        NativeScriptUICalendarModule,
        // AngularFireModule,
        // AngularFirestoreModule
    ],
    declarations: [
        AppComponent,
        MyButtonComponent,
        MyHomeComponent,
        MyQuizComponent,
      MyCalenderComponent,
      MyWorkComponent,
      MyUniComponent
   ],
    providers: [],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
/*
Pass your application module to the bootstrapModule function located in main.ts to start your app
*/
export class AppModule {
}
