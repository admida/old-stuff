import { Component, OnInit } from '@angular/core';
import { setInterval, setTimeout } from 'tns-core-modules/timer';

// * Firebase Imports
const firebase = require("nativescript-plugin-firebase");
require('firebase/firestore');
import 'firebase/firebase-app';
import 'firebase/firebase-storage';

@Component({
  selector: 'ns-my-work',
  templateUrl: './my-work.component.html',
  styleUrls: ['./my-work.component.scss']
})
export class MyWorkComponent implements OnInit {
    inputValue: number = 0;

    seconds: number = 0;
    minutes: number = 0;
    hours: number = 0;

    interval: any;
    isRunning = false;

    constructor() { }

    onInputChange(inputValue: number) {
    this.inputValue = inputValue;
    }

    onStart() {
        console.log(this.inputValue);
        const intervalFunc = () => {
                this.inputValue = this.inputValue + 1;
                console.log(this.inputValue)

                if(this.inputValue < 60){
                    // this.hours = 0;
                    this.minutes = 0;
                    this.seconds = this.inputValue;
                } else if (this.inputValue > 60 && this.inputValue < 3600) {
                    // this.hours = 0;
                    this.minutes = Math.floor(this.inputValue / 60);
                    this.seconds = this.inputValue%60;
                } else if (this.inputValue > 3600) {
                    this.hours = this.hours + 1;
                    this.inputValue = 0;
                    this.onAddTime(3600)
                }
        }
            if(this.interval) {
                clearInterval(this.interval);
            }
            intervalFunc();
            this.interval = setInterval(intervalFunc, 1000)
    }

    onStop(){
        //* writing on Firestore
        this.onAddTime(this.inputValue);
        //* clear Data
        this.inputValue = 0;
        clearInterval(this.interval);
    }

    // * Firestore
    db: any;

    ngOnInit() {
        firebase.init({
        }).then(
            () => {
                console.log("firebase.init done")
            },
            error => {
                console.log("firebase already init");
            }
        );
    }

    // * getDate

    todayString: string;
    tryNumber: number = 4000;

    onGetDate() {
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();

        this.todayString = dd + '-' + mm + '-' + yyyy;
        // this.todayString = yyyy + '.' + mm + '.' + dd;
        return(this.todayString);
    }

    // * Writing on Firestore

    onAddTime(timeToAdd: number){
        var db2 = firebase.firestore;
        var docRef2 = db2.collection("test").doc("SFYzxm7F7dt6O4HQWqyg");

        var i: string = this.onGetDate();

        docRef2.get().then(function(doc) {
            if (doc.data()[i] != null) {
                console.log("Document data:", doc.data()[i]);
                //! read old Data
                var x = doc.data()[i];
                x = x + timeToAdd;

                //! update Data
                return docRef2.update({
                    [i]: x
                })
            } else {
                return docRef2.set({
                    [i]: timeToAdd
                }, { merge: true });
            }
        }).catch(function(error) {
            console.log("Error getting document:", error);
        });
    }
}

