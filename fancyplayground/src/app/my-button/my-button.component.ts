import { Component, Input } from '@angular/core';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';


@Component({
  selector: 'ns-my-button',
  templateUrl: './my-button.component.html',
  styleUrls: ['./my-button.component.css']
})
export class MyButtonComponent {
    /* @Input() color: string;
    @Input() styling: string;
    @Input() disabled: boolean; */

    @Input() linkTo: string;

    constructor() {}

    ngOnInit() {}

    onTap() {
        console.log("hi");
        // var link = this.linkTo;
    }

}
