import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ns-my-quiz',
  templateUrl: './my-quiz.component.html',
  styleUrls: ['./my-quiz.component.scss']
})
export class MyQuizComponent implements OnInit {
    random: number;
    extra_class: string;

    qst= [
        {id: 1, txt: "What is the capital of Chile?", a1: "Santiago", a2: "Berlin", a3: "Casablanca", a4: "Honduras", ra: 1},
        {id: 2, txt: "lol"}
    ]

  constructor() { }

  ngOnInit() {
  }

  onRandom() {
      this.random = Math.floor((Math.random() * 3) + 1);
      console.log(this.random);
  }

  onCheck(ans_given){
      if(ans_given === this.qst[0].ra){
          this.extra_class = "correct";
      } else {
          this.extra_class = "false";
      }
  }
}
